This project is saved on the public repository: [`https://gitlab.com/andreiastark/pokemon-team-management`](https://gitlab.com/andreiastark/pokemon-team-management).

## Introduction

**Pokémon Team Management** is based on **Next.js** for the front-end and back-end part.
For the DataBase uses **MongoDb**. During development it is possible to use **Mongo Express** as a graphical interface to view the DB data.
The **SWR** library is used for cache management.
The **docker-compose** starts and coordinates all instances necessary for the application to run correctly.

## Screenshot

### Dashboard

![alt text1][dashboard]

[dashboard]: readme_img/dashboard.png "Dashboard"

### Team List

![alt text1][team_list]

[team_list]: readme_img/team_list.png "Team List"

### Create Team

![alt text1][create_team]

[create_team]: readme_img/create_team.png "Create Team"

### Edit Team

![alt text1][edit_team]

[edit_team]: readme_img/edit_team.png "Edit Team"

## Getting Started

Here are the steps to set up the project:

- Rename the **example.env** file to **.env** and set the variables. For example you can configure the file like this:

```bash
MONGO_ROOT_USERNAME="pokemonTeamManagementRoot"
MONGO_ROOT_PASSWORD="Cj%q8L4@gGE0bnnKIBiY"

APP_CONTAINER_NAME="pokemonTeamManagement"
MONGO_CONTAINER_NAME="mongoDb"
MONGO_EXPRESS_CONTAINER_NAME="mongoInterface"

APP_LOCAL_PORT="9876"
MONGO_LOCAL_PORT="9877"
MONGO_EXPRESS_LOCAL_PORT="9878"

MONGO_CONTAINER_PORT="27017"
```

- Run npm install:

```bash
npm install
```

- Run Docker Compose:

```bash
docker-compose up
```

## Structure of the Pages/Components

```mermaid
graph TB
   subgraph "Dashboard"
       pages/index.js
   end

   subgraph "Team List"
       pages/index.js --> pages/team/list.js
       pages/team/list.js --> components/pokemon/team-data-table.js
       components/pokemon/team-data-table.js --> components/pokemon/pokemon-types.js
       components/pokemon/team-data-table.js --> components/pokemon/pokemon-delete-button.js
       components/pokemon/team-data-table.js --> components/pokemon/team-data-filters.js
       components/pokemon/team-data-filters.js --> components/pokemon/select-types.js
   end

   subgraph "Team Create/Edit"
       pages/index.js --> pages/team/create.js
       pages/team/create.js --> components/pokemon/pokemon-form.js
       pages/team/id/edit.js --> components/pokemon/pokemon-form.js
       components/pokemon/pokemon-form.js --> components/pokemon/pokemon-selector.js
       components/pokemon/pokemon-selector.js --> components/pokemon/pokemon-card.js
       components/pokemon/pokemon-card.js --> components/pokemon/pokemon-image.js
       components/pokemon/pokemon-card.js --> components/pokemon/pokemon-abilities.js
       components/pokemon/pokemon-card.js --> components/pokemon/pokemon-types.js
   end
```

## API

- pages/api/pokemon/v1:
  - **GET** -> "/api/pokemon/v1/get-random-pokemon" ( returns a random Pokémon )
  - **GET** -> "/api/pokemon/v1/types" ( returns the list of all types of Pokémon )
- pages/api/team/v1:
  - **GET** -> "/api/team/v1" ( returns all teams )
  - **POST** -> "/api/team/v1" ( create a new team )
  - **GET** -> "/api/team/v1/id" ( returns a team with a certain id )
  - **PUT** -> "/api/team/v1/id" ( update a team with a certain id )
  - **DELETE** -> "/api/team/v1/id" ( remove a team with a certain id )
