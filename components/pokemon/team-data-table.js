import { DataGrid } from "@mui/x-data-grid";
import Image from "next/image";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";
import Link from "next/link";
import PokemonTypes from "./pokemon-types";
import PokemonDeleteButton from "./pokemon-delete-button";
import TeamDataFilters from "./team-data-filters";

const PokemonImages = (params) => {
  const formattedValue = params.params.formattedValue;
  const images =
    formattedValue != undefined
      ? params.params.formattedValue.map((item) => item.img)
      : [];

  const imgLoader = ({ src, width, quality }) => {
    return `${src}?w=${width}&q=${quality || 75}`;
  };

  return (
    <div>
      {images.map((img, index) => (
        <Image
          loader={imgLoader}
          key={img}
          src={img ? img : "/images/type_unknown.png"}
          alt="Picture of the author"
          width={50}
          height={50}
        />
      ))}
    </div>
  );
};

const PokemonTypesInTable = (params) => {
  const pokemonList = params.params.row.pokemon;
  let types = [];
  pokemonList.map((item) => (types = [...types, ...item.types]));
  return (
    <div>
      <PokemonTypes pokemonTypes={[...new Set(types)]} />
    </div>
  );
};

const PokemonTableActionButton = (params) => {
  const id = params.params.id;

  return (
    <div>
      <Link href={`/team/${id}/edit`}>
        <IconButton color="primary" aria-label="edit team">
          <EditIcon />
        </IconButton>
      </Link>
      <PokemonDeleteButton teamId={id} />
    </div>
  );
};

const columns = [
  { field: "_id", headerName: "ID", hide: true },
  {
    field: "action",
    headerName: "Action",
    width: 100,
    renderCell: (params) => <PokemonTableActionButton params={params} />,
    sortable: false,
  },
  { field: "teamName", headerName: "Team Name", width: 250 },
  {
    field: "pokemon",
    headerName: "Pokemon",
    width: 350,
    renderCell: (params) => <PokemonImages params={params} />,
    sortable: false,
  },
  {
    field: "base_experiences_sum",
    headerName: "Base Experiences Sum",
    type: "number",
    width: 250,
    valueGetter: (params) =>
      `${(params.row.pokemon || [])
        .map((item) => item.base_experience)
        .reduce((partialSum, a) => partialSum + a, 0)}`,
  },
  {
    field: "types",
    headerName: "Types",
    width: 400,
    renderCell: (params) => <PokemonTypesInTable params={params} />,
    sortable: false,
  },
  {
    field: "createdAt",
    headerName: "Created At",
    width: 250,
    valueGetter: (params) =>
      `${new Date(params.row.createdAt).toLocaleString()}`,
  },
];

export default function TeamDataTable({ team }) {
  const rows = team;

  return (
    <>
      <TeamDataFilters />
      <div style={{ height: 750, width: "100%" }}>
        <div style={{ display: "flex", height: "100%" }}>
          <div style={{ flexGrow: 1 }}>
            <DataGrid
              initialState={{
                sorting: {
                  sortModel: [{ field: "createdAt", sort: "desc" }],
                },
              }}
              rows={rows}
              getRowHeight={() => "auto"}
              disableColumnMenu={true}
              columns={columns}
              pageSize={10}
              rowsPerPageOptions={[10]}
              getRowId={(row) => row._id}
            />
          </div>
        </div>
      </div>
    </>
  );
}
