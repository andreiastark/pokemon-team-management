import styles from "../../styles/pokemon-card.module.css";
import Box from "@mui/material/Box";
import CardMedia from "@mui/material/CardMedia";

export default function PokemonImage({
  pokemonImage,
  pokemonTypes,
  pokemonName,
}) {
  return (
    <>
      <Box className={styles.list}>
        <div
          className={styles.listCard}
          data-slug="taillow"
          data-type={pokemonTypes.join(",")}
        >
          <div className={styles.wrap}>
            <figure>
              <CardMedia
                component="img"
                alt={pokemonName}
                image={pokemonImage}
              />
            </figure>
          </div>
        </div>
      </Box>
    </>
  );
}
