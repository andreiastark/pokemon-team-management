import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Box from "@mui/material/Box";
import BrightnessHighIcon from "@mui/icons-material/BrightnessHigh";

export default function PokemonAbilities({ pokemonAbilities }) {
  return (
    <Box height={125}>
      <List>
        {pokemonAbilities.map(function (ability, index) {
          return (
            <ListItem key={ability} disablePadding>
              <ListItemIcon>
                <BrightnessHighIcon />{" "}
              </ListItemIcon>
              <ListItemText primary={ability} />
            </ListItem>
          );
        })}
      </List>
    </Box>
  );
}
