import PropTypes from "prop-types";
import Button from "@mui/material/Button";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import { useState } from "react";
import { useSWRConfig } from "swr";
import {
  useStateStoreContext,
  setServiceMessage,
} from "../../context/stateStore";

function ConfirmationDialogRaw(props) {
  const { onClose, open, teanId, ...other } = props;

  const context = useStateStoreContext();
  const { mutate } = useSWRConfig();

  const handleDeleteTeam = async (id) => {
    let urlForm = `/api/team/v1/${id}`;
    let method = "DELETE";

    const options = {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(urlForm, options);

      context.setLoaderState(true);

      setTimeout(() => {
        mutate("/api/team/v1");
        context.setLoaderState(false);
      }, 2000);
    } catch (error) {
      setServiceMessage({
        context: context,
        text: error,
        type: "error",
      });
    }
  };

  const handleCancel = () => {
    onClose();
  };

  const handleOk = (id) => {
    handleDeleteTeam(id);
    onClose();
  };

  return (
    <Dialog
      sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
      maxWidth="xs"
      open={open}
      {...other}
    >
      <DialogTitle>Delete Team</DialogTitle>
      <DialogContent dividers>
        Are you sure you want to remove this team?
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleCancel}>
          No
        </Button>
        <Button onClick={() => handleOk(teanId)}>Yes</Button>
      </DialogActions>
    </Dialog>
  );
}

ConfirmationDialogRaw.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function PokemonDeleteButton({ teamId }) {
  const [open, setOpen] = useState(false);

  const handleClickListItem = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <IconButton
        onClick={() => handleClickListItem()}
        color="error"
        aria-label="delete team"
        component="label"
      >
        <DeleteIcon />
      </IconButton>

      <ConfirmationDialogRaw
        id="delete-team"
        teanId={teamId}
        keepMounted
        open={open}
        onClose={handleClose}
      />
    </>
  );
}
