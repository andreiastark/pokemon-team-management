import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { useState, useEffect } from "react";
import PokemonSelector from "./pokemon-selector";
import { useRouter } from "next/router";
import {
  useStateStoreContext,
  setServiceMessage,
} from "../../context/stateStore";
import ButtonGroup from "@mui/material/ButtonGroup";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

import SaveIcon from "@mui/icons-material/Save";

export default function PokemonForm({ formType, team }) {
  const [pokemonList, setPokemonList] = useState([]);
  const [teamName, setTeamName] = useState("");
  const [teamNameError, setTeamNameError] = useState(false);

  const context = useStateStoreContext();

  const router = useRouter();

  useEffect(() => {
    if (formType == "update") {
      setTeamName(team.teamName);
      setPokemonList(team.pokemon);
    }
  }, [team]);

  const handleSubmit = async (formType) => {
    let urlForm = "/api/team/v1";
    let method = "POST";

    if (formType == "update") {
      urlForm = `/api/team/v1/${team._id}`;
      method = "PUT";
    }

    if (teamName == "") {
      setTeamNameError(true);
      setServiceMessage({
        context: context,
        text: "The 'Team Name' field cannot be empty.",
        type: "error",
      });
      return false;
    } else {
      setTeamNameError(false);
    }

    const data = {
      teamName: teamName,
      pokemon: pokemonList,
    };

    const JSONdata = JSON.stringify(data);

    const options = {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSONdata,
    };

    try {
      const response = await fetch(urlForm, options);

      setServiceMessage({
        context: context,
        text: "Saving completed!",
        type: "success",
      });

      router.push("/team/list");
    } catch (error) {
      setServiceMessage({
        context: context,
        text: error.message,
        type: "error",
      });
    }
  };

  return (
    <>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        marginBottom={5}
      >
        <TextField
          error={teamNameError}
          required
          id="teamName"
          style={{ width: "400px", margin: "5px" }}
          type="text"
          label="Enter the team name"
          variant="outlined"
          onChange={(event) => {
            setTeamName(event.target.value);
          }}
          value={teamName}
          helperText={teamNameError ? "This field cannot be empty" : ""}
        />
      </Grid>

      <PokemonSelector
        pokemonList={pokemonList}
        setPokemonList={setPokemonList}
      />

      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        marginBottom={5}
      >
        <ButtonGroup
          variant="contained"
          aria-label="Disabled elevation buttons"
        >
          <Button
            onClick={() => router.push("/team/list")}
            variant="contained"
            size="large"
            color="neutral"
          >
            <ArrowBackIcon /> Back
          </Button>
          <Button
            onClick={() => handleSubmit(formType)}
            variant="contained"
            size="large"
            color="success"
          >
            <SaveIcon /> Save
          </Button>
        </ButtonGroup>
      </Grid>
    </>
  );
}
