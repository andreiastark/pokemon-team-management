import { useState } from "react";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import PokemonCard from "./pokemon-card";
import {
  useStateStoreContext,
  setServiceMessage,
} from "../../context/stateStore";

export default function PokemonSelector({ pokemonList, setPokemonList }) {
  const [addPokermonButtonDisabled, setAddPokermonButtonDisabled] =
    useState(false);

  const context = useStateStoreContext();

  const getRandomPokemon = async () => {
    setAddPokermonButtonDisabled(true);
    context.setLoaderState(true);
    // API endpoint where we send form data.
    const endpoint = "/api/pokemon/v1/get-random-pokemon";

    // Form the request for sending data to the server.
    const options = {
      // The method is POST because we are sending data.
      method: "GET",
      // Tell the server we're sending JSON.
      headers: {
        "Content-Type": "application/json",
      },
    };

    if (pokemonList.length >= 6) {
      setServiceMessage({
        context: context,
        text: "The team is complete, it is not possible to add more than 6 Pokémon.",
        type: "error",
      });
    } else {
      try {
        const response = await fetch(endpoint, options);

        const result = await response.json();
        const pokemonItem = {
          name: result.name,
          img: result.sprites.other["official-artwork"].front_default,
          base_experience: result.base_experience,
          abilities: result.abilities.map((ability) => ability.ability.name),
          types: result.types.map((type) => type.type.name),
        };
        setPokemonList((current) => [...current, pokemonItem]);
      } catch (error) {
        setServiceMessage({
          context: context,
          text: error.message,
          type: "error",
        });
      }
    }
    setAddPokermonButtonDisabled(false);
    context.setLoaderState(false);
  };

  return (
    <>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        marginBottom={5}
      >
        <Typography marginRight={3} variant="h4" gutterBottom>
          Select a Pokémon:
        </Typography>
        <Button
          onClick={getRandomPokemon}
          variant="contained"
          color="primary"
          disabled={addPokermonButtonDisabled}
        >
          Gotta Catch 'Em All
        </Button>
      </Grid>

      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        marginBottom={5}
        spacing={1}
        columns={{ xs: 2, sm: 2, md: 6 }}
      >
        {[...Array(6).keys()].map((pokemon, index) => (
          <Grid item key={pokemon} xs={1} sm={1} md={1}>
            <PokemonCard
              pokemonList={pokemonList}
              setPokemonList={setPokemonList}
              pokemon={pokemonList[index]}
            ></PokemonCard>
          </Grid>
        ))}
      </Grid>
    </>
  );
}
