import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import PokemonTypes from "./pokemon-types";
import PokemonImage from "./pokemon-image";
import PokemonAbilities from "./pokemon-abilities";
import styles from "../../styles/pokemon-card.module.css";

export default function PokemonCard({ pokemon, pokemonList, setPokemonList }) {
  let img = pokemon && pokemon.img ? pokemon.img : "/images/type_unknown.png";
  let name = pokemon ? pokemon.name : "???";
  let base_experience = pokemon ? pokemon.base_experience : "???";
  let abilities = pokemon ? pokemon.abilities : ["???"];
  let types = pokemon ? pokemon.types : ["???"];

  const removePokemon = (pokemonName) => {
    let clonedArray = JSON.parse(JSON.stringify(pokemonList));
    clonedArray.splice(
      clonedArray.findIndex(({ name }) => name == pokemonName),
      1
    );

    setPokemonList(clonedArray);
  };

  return (
    <>
      <br></br>
      <Card
        sx={{ maxWidth: 200, minHeight: 570 }}
        sm={{ maxWidth: 400, minHeight: 570 }}
        md={{ maxWidth: 400, minHeight: 570 }}
      >
        <PokemonImage
          pokemonImage={img}
          pokemonTypes={types}
          pokemonName={name}
        />

        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {name}
          </Typography>

          <Typography variant="button" display="block" gutterBottom>
            Base Experience
          </Typography>
          {base_experience}

          <Typography variant="button" display="block" gutterBottom>
            Abilities
          </Typography>
          <PokemonAbilities pokemonAbilities={abilities} />

          <Typography variant="button" display="block" gutterBottom>
            Types
          </Typography>
          <Box height={50}>
            <PokemonTypes pokemonTypes={types} />
          </Box>
        </CardContent>
        <CardActions>
          {name == "???" ? (
            ""
          ) : (
            <Button
              onClick={() => removePokemon(name)}
              className={styles.removePokemon}
              variant="contained"
              size="small"
              color="error"
            >
              Remove
            </Button>
          )}
        </CardActions>
      </Card>
    </>
  );
}
