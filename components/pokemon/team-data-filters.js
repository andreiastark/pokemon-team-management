import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import ButtonGroup from "@mui/material/ButtonGroup";
import Button from "@mui/material/Button";
import Badge from "@mui/material/Badge";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import SelectTypes from "./select-types";
import { useState } from "react";
import { useStateStoreContext } from "../../context/stateStore";
import { useSWRConfig } from "swr";
import AddBoxIcon from "@mui/icons-material/AddBox";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import { useRouter } from "next/router";

function ConfirmationDialogRaw(props) {
  const {
    onClose,
    open,
    typesFilterState,
    setTypesFilterState,
    tmpTypesFilterState,
    ...other
  } = props;

  const { mutate } = useSWRConfig();
  const context = useStateStoreContext();

  const handleCancel = () => {
    onClose();
  };

  const handleOk = () => {
    context.setLoaderState(true);
    setTypesFilterState(tmpTypesFilterState);

    setTimeout(() => {
      mutate("/api/team/v1");
      context.setLoaderState(false);
    }, 2000);

    onClose();
  };

  return (
    <Dialog
      sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
      maxWidth="xs"
      open={open}
      {...other}
    >
      <DialogTitle>Types Filter</DialogTitle>
      <DialogContent dividers>
        <SelectTypes />
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleCancel}>
          Cancel
        </Button>
        <Button onClick={handleOk}>Ok</Button>
      </DialogActions>
    </Dialog>
  );
}

ConfirmationDialogRaw.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default function TeamDataFilters() {
  const [open, setOpen] = useState(false);
  const router = useRouter();

  const { tmpTypesFilterState, typesFilterState, setTypesFilterState } =
    useStateStoreContext();

  const handleClickListItem = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ width: "100%", bgcolor: "background.paper" }}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        marginBottom={5}
      >
        <ButtonGroup
          variant="contained"
          aria-label="outlined primary button group"
        >
          <Button
            onClick={() => router.push("/team/create")}
            startIcon={<AddBoxIcon />}
            variant="contained"
            color="success"
          >
            Add Team
          </Button>
          <Badge badgeContent={typesFilterState.length} color="success">
            <Button
              variant="contained"
              startIcon={<FilterAltIcon />}
              color="primary"
              onClick={handleClickListItem}
            >
              Types Filter
            </Button>
          </Badge>
        </ButtonGroup>
      </Grid>

      <ConfirmationDialogRaw
        id="typesFilterBox"
        keepMounted
        open={open}
        setTypesFilterState={setTypesFilterState}
        tmpTypesFilterState={tmpTypesFilterState}
        onClose={handleClose}
      />
    </Box>
  );
}
