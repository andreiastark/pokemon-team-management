import { useState, useEffect } from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";
import { useStateStoreContext } from "../../context/stateStore";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, tmpTypesFilterState, theme) {
  return {
    fontWeight:
      tmpTypesFilterState.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function SelectTypes() {
  const theme = useTheme();
  const [pokemonTypes, setPokemonTypes] = useState([]);
  const { tmpTypesFilterState, setTmpTypesFilterState, typesFilterState } =
    useStateStoreContext();

  useEffect(() => {
    fetch("/api/pokemon/v1/types")
      .then((res) => res.json())
      .then((data) => {
        setPokemonTypes(data.map((item) => item.name).sort());
      });
    setTmpTypesFilterState(typesFilterState);
  }, [typesFilterState]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setTmpTypesFilterState(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  return (
    <div>
      <FormControl sx={{ m: 1, width: 350 }}>
        <InputLabel id="demo-multiple-chip-label">Types</InputLabel>
        <Select
          labelId="demo-multiple-chip-label"
          id="demo-multiple-chip"
          multiple
          value={tmpTypesFilterState}
          onChange={handleChange}
          input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
          renderValue={(selected) => (
            <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
          MenuProps={MenuProps}
        >
          {pokemonTypes.map((type) => (
            <MenuItem
              key={type}
              value={type}
              style={getStyles(type, tmpTypesFilterState, theme)}
            >
              {type}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
