import styles from "../../styles/pokemon-card.module.css";

export default function PokemonTypes({ pokemonTypes }) {
  return (
    <>
      <div className={`${styles.list}`}>
        <div className={styles.listCard}>
          <div className={`${styles.info}`}>
            {pokemonTypes.map((type) => {
              return (
                <span key={type} className={`${styles.type} ${styles[type]}`}>
                  {type}
                </span>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}
