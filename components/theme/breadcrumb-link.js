import Link from "@mui/material/Link";

export default function LayoutBreadcrumbLink({ menu }) {
  return (
    <Link
      underline="hover"
      sx={{ display: "flex", alignItems: "center" }}
      color="inherit"
      href={menu.link}
    >
      {menu.icon}
      {menu.breadcrumbLabel}
    </Link>
  );
}
