import Breadcrumbs from "@mui/material/Breadcrumbs";
import menuList from "../../src/menuList";
import LayoutBreadcrumbLink from "./breadcrumb-link";
import LayoutBreadcrumbCurrent from "./breadcrumb-current";
import { useRouter } from "next/router";

export default function LayoutBreadcrumbs() {
  const router = useRouter();
  let currentPage = false;

  return (
    <div role="presentation">
      <Breadcrumbs aria-label="breadcrumb">
        {menuList.map((item, index) => {
          if (currentPage) {
            return "";
          } else {
            if (item.link == router.pathname) {
              currentPage = true;
            }
            return item.link == router.pathname || item.breadcrumbFinal ? (
              <LayoutBreadcrumbCurrent key={item.label} menu={item} />
            ) : (
              <LayoutBreadcrumbLink key={item.label} menu={item} />
            );
          }
        })}
      </Breadcrumbs>
    </div>
  );
}
