import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { useStateStoreContext } from "../../context/stateStore";

export default function Loader() {
  const { loaderState, setLoaderState } = useStateStoreContext();

  return (
    <div>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loaderState}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}
