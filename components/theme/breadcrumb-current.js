import Typography from "@mui/material/Typography";

export default function LayoutBreadcrumbCurrent({ menu }) {
  return (
    <Typography
      sx={{ display: "flex", alignItems: "center" }}
      color="text.primary"
    >
      {menu.icon}
      {menu.breadcrumbLabel}
    </Typography>
  );
}
