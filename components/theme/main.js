import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import LayoutBreadcrumbs from "./breadcrumbs";

const drawerWidth = 240;

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

export default function Main({ children }) {
  return (
    <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
      <DrawerHeader />
      <LayoutBreadcrumbs />
      {children}
    </Box>
  );
}
