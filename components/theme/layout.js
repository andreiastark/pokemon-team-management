import Box from "@mui/material/Box";
import Appbar from "./appbar";
import DrawerMenu from "./drawer";
import Main from "./main";

export default function Layout({ children }) {
  return (
    <Box sx={{ display: "flex" }}>
      <Appbar></Appbar>
      <DrawerMenu></DrawerMenu>
      <Main children={children}></Main>
    </Box>
  );
}
