import * as React from "react";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import { useStateStoreContext } from "../../context/stateStore";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function ServiceMessage() {
  const { serviceMessageState, setServiceMessageState } =
    useStateStoreContext();
  const { serviceMessageTextState, setServiceMessageTextState } =
    useStateStoreContext();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setServiceMessageState(false);
  };

  return (
    <>
      <Snackbar
        open={serviceMessageState}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert
          onClose={handleClose}
          severity={serviceMessageTextState.type}
          sx={{ width: "100%" }}
        >
          {serviceMessageTextState.text}
        </Alert>
      </Snackbar>
    </>
  );
}
