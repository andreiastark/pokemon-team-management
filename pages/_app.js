import PropTypes from "prop-types";
import Head from "next/head";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider } from "@emotion/react";
import theme from "../src/theme";
import createEmotionCache from "../src/createEmotionCache";
import Layout from "../components/theme/layout";
import Loader from "../components/theme/loader";
import ServiceMessage from "../components/theme/serviceMessage";
import { SWRConfig } from "swr";

import { StateStoreProvider } from "../context/stateStore";

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

export default function MyApp(props) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <StateStoreProvider>
      <CacheProvider value={emotionCache}>
        <Head>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
        </Head>
        <ThemeProvider theme={theme}>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <Loader />
          <ServiceMessage />
          <Layout>
            <SWRConfig value={{ provider: () => new Map() }}>
              <Component {...pageProps} />
            </SWRConfig>
          </Layout>
        </ThemeProvider>
      </CacheProvider>
    </StateStoreProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  emotionCache: PropTypes.object,
  pageProps: PropTypes.object.isRequired,
};
