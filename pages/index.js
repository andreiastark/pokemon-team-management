import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import CardActionArea from "@mui/material/CardActionArea";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();

  const cardList = [
    {
      name: "Team List",
      image: "/images/pokedex.png",
      link: "/team/list",
      text: "This section contains the list of all the Pokémon teams you have created.",
    },
    {
      name: "Add Team",
      image: "/images/add_pokemon.jpeg",
      link: "/team/create",
      text: "Add new Pokémon teams to your collection!",
    },
  ];

  return (
    <Box sx={{ width: "100%" }}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <Typography variant="h2" gutterBottom>
          Dashboard
        </Typography>
      </Grid>

      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        marginBottom={5}
        marginTop={10}
        spacing={4}
        columns={{ xs: 1, sm: 1, md: 4 }}
      >
        {cardList.map((item, index) => (
          <Grid item key={item.name} xs={1} sm={1} md={1}>
            <Card>
              <CardActionArea onClick={() => router.push(item.link)}>
                <CardMedia
                  component="img"
                  height="140"
                  image={item.image}
                  alt={item.name}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    {item.name}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {item.text}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}
