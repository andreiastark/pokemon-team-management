const getRandomIntInclusive = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
};

export default async function handler(req, res) {
  const endpoint =
    "https://pokeapi.co/api/v2/pokemon/" + getRandomIntInclusive(1, 905);

  const options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };

  switch (req.method) {
    case "GET":
      try {
        const response = await fetch(endpoint, options);
        const result = await response.json();
        res.status(200).json(result);
      } catch (error) {
        res.status(400).send(error);
      }
      break;
    default:
      res.status(405).end();
      break;
  }
}
