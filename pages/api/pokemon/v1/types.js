export default async function handler(req, res) {
  const endpoint = "https://pokeapi.co/api/v2/type";

  const options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };

  switch (req.method) {
    case "GET":
      try {
        const response = await fetch(endpoint, options);
        const result = await response.json();
        res.status(200).json(result.results);
      } catch (error) {
        res.status(400).send(error);
      }
      break;
    default:
      res.status(405).end();
      break;
  }
}
