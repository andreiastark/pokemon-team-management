import { clientPromise, ObjectId } from "../../../../lib/mongodb";

export default async function handler(req, res) {
  const client = await clientPromise;
  const db = client.db(process.env.APP_CONTAINER_NAME + "Db");
  const collection = "team";

  let { id } = req.query;

  switch (req.method) {
    case "GET":
      try {
        let teamGet = await db.collection(collection).findOne(ObjectId(id));
        res.status(200).json(teamGet);
      } catch (error) {
        res.status(400).send(error);
      }

      break;
    case "PUT":
      let bodyObject = req.body;

      try {
        let teamUpdate = db.collection(collection).update(
          { _id: ObjectId(id) },
          {
            $set: bodyObject,
          }
        );
        res.status(200).json(teamUpdate);
      } catch (error) {
        res.status(400).send(error);
      }

      break;
    case "DELETE":
      try {
        let teamDelete = db
          .collection(collection)
          .deleteOne({ _id: ObjectId(id) });
        res.status(200).json(teamDelete);
      } catch (error) {
        res.status(400).send(error);
      }

      break;
    default:
      res.status(405).end();
      break;
  }
}
