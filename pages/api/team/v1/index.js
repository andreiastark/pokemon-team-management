import { clientPromise } from "../../../../lib/mongodb";

export default async function handler(req, res) {
  const client = await clientPromise;
  const db = client.db(process.env.APP_CONTAINER_NAME + "Db");

  switch (req.method) {
    case "POST":
      let bodyObject = req.body;
      bodyObject.createdAt = new Date();

      try {
        let team = await db.collection("team").insertOne(bodyObject);
        res.json(team);
      } catch (error) {
        res.status(400).send(error);
      }

      break;
    case "GET":
      let types = req.query["types[]"] || [];

      types = Array.isArray(types) ? types : [types];

      let query = {};
      if (types.length != 0) {
        query = { "pokemon.types": { $all: types } };
      }
      try {
        console.log(query);
        const allTeam = await db.collection("team").find(query).toArray();
        res.status(200).json(allTeam);
      } catch (error) {
        res.status(400).send(error);
      }

      break;
  }
}
