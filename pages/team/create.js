import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import PokemonForm from "../../components/pokemon/pokemon-form";

export default function TeamCreate() {
  return (
    <Box sx={{ width: "100%" }}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <Typography variant="h2" gutterBottom>
          Create new Team
        </Typography>
      </Grid>

      <PokemonForm formType="create" />
    </Box>
  );
}
