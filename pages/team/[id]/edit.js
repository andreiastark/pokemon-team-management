import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import PokemonForm from "../../../components/pokemon/pokemon-form";
import useSWR from "swr";

const fetcher = (...args) => fetch(...args).then((res) => res.json());

export default function TeamEdit({ id }) {
  const { data } = useSWR(`/api/team/v1/${id}`, fetcher);
  return (
    <Box sx={{ width: "100%" }}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <Typography variant="h2" gutterBottom>
          Edit Team
        </Typography>
      </Grid>
      {data == undefined ? (
        "Loading"
      ) : (
        <PokemonForm formType="update" team={data} />
      )}
    </Box>
  );
}

export function getServerSideProps(context) {
  const { id } = context.query;
  return {
    props: { id: id },
  };
}
