import axios from "axios";
import useSWR from "swr";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

import TeamDataTable from "../../components/pokemon/team-data-table";
import { useStateStoreContext } from "../../context/stateStore";

export default function TeamList() {
  const { typesFilterState } = useStateStoreContext();

  const address = `/api/team/v1`;
  const fetcher = async (url) =>
    await axios
      .get(url, { params: { types: typesFilterState } })
      .then((res) => res.data);
  const { data } = useSWR(address, fetcher);

  return (
    <Box sx={{ width: "100%" }}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <Typography variant="h2" gutterBottom>
          Team List
        </Typography>
      </Grid>

      <TeamDataTable team={data && data ? data : []}></TeamDataTable>
    </Box>
  );
}
