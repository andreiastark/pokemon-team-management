import HomeIcon from "@mui/icons-material/Home";
import ListAltIcon from "@mui/icons-material/ListAlt";
import AddBoxIcon from "@mui/icons-material/AddBox";

const menuList = [
  {
    label: "Home",
    breadcrumbLabel: "Dashboard",
    icon: <HomeIcon />,
    link: "/",
    breadcrumbFinal: false,
  },
  {
    label: "Team List",
    breadcrumbLabel: "Team",
    icon: <ListAltIcon />,
    link: "/team/list",
    breadcrumbFinal: false,
  },
  {
    label: "Add Team",
    breadcrumbLabel: "Form",
    icon: <AddBoxIcon />,
    link: "/team/create",
    breadcrumbFinal: true,
  },
];

export default menuList;
