import { createContext, useContext, useState } from "react";

const StateStoreContext = createContext();

export function StateStoreProvider({ children }) {
  const [drawerState, setDrawerState] = useState(true);
  const [loaderState, setLoaderState] = useState(false);
  const [serviceMessageState, setServiceMessageState] = useState(false);
  const [serviceMessageTextState, setServiceMessageTextState] = useState(false);
  const [tmpTypesFilterState, setTmpTypesFilterState] = useState([]);
  const [typesFilterState, setTypesFilterState] = useState([]);
  return (
    <StateStoreContext.Provider
      value={{
        drawerState,
        setDrawerState,
        loaderState,
        setLoaderState,
        serviceMessageState,
        setServiceMessageState,
        serviceMessageTextState,
        setServiceMessageTextState,
        tmpTypesFilterState,
        setTmpTypesFilterState,
        typesFilterState,
        setTypesFilterState,
      }}
    >
      {children}
    </StateStoreContext.Provider>
  );
}

export function useStateStoreContext() {
  const context = useContext(StateStoreContext);

  if (!context)
    throw new Error(
      "useStateStoreContext must be used inside a `StateStoreProvider`"
    );

  return context;
}

export function setServiceMessage({ context, text, type }) {
  context.setServiceMessageState(true);
  context.setServiceMessageTextState({ text: text, type: type });
}
